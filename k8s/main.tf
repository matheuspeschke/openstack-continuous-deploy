module "admnode" {
  source = "./admnode"

  user = var.user
  home = var.home
  sshkeypath = var.sshkeypath

  imageid = var.imageid
  flavorname = var.flavorname
  keypair = "default"
  securitygroups = ["default"]

  nodes = var.nodes
  clusternodes = var.clusternodes

  admnode = var.admnode
  admnodepubip = var.admnodepubip
  cidr = var.cidr
}

module "node002" {
  source = "./k8snode"
  nodename = keys(var.nodes)[0]
  nodeip = values(var.nodes)[0]

  user = var.user
  home = var.home
  sshkeypath = var.sshkeypath

  imageid = var.imageid
  flavorname = var.flavorname
  keypair = "default"
  securitygroups = ["default"]

  clusternodes = var.clusternodes
}

module "node003" {
  source = "./k8snode"
  nodename = keys(var.nodes)[1]
  nodeip = values(var.nodes)[1]

  user = var.user
  home = var.home
  sshkeypath = var.sshkeypath

  imageid = var.imageid
  flavorname = var.flavorname
  keypair = "default"
  securitygroups = ["default"]

  clusternodes = var.clusternodes
}

module "node004" {
  source = "./k8snode"
  nodename = keys(var.nodes)[2]
  nodeip = values(var.nodes)[2]

  user = var.user
  home = var.home
  sshkeypath = var.sshkeypath

  imageid = var.imageid
  flavorname = var.flavorname
  keypair = "default"
  securitygroups = ["default"]

  clusternodes = var.clusternodes
}

module "node005" {
  source = "./k8snode"
  nodename = keys(var.nodes)[3]
  nodeip = values(var.nodes)[3]

  user = var.user
  home = var.home
  sshkeypath = var.sshkeypath

  imageid = var.imageid
  flavorname = var.flavorname
  keypair = "default"
  securitygroups = ["default"]

  clusternodes = var.clusternodes
}

module "node006" {
  source = "./k8snode"
  nodename = keys(var.nodes)[4]
  nodeip = values(var.nodes)[4]

  user = var.user
  home = var.home
  sshkeypath = var.sshkeypath

  imageid = var.imageid
  flavorname = var.flavorname
  keypair = "default"
  securitygroups = ["default"]

  clusternodes = var.clusternodes
}

module "node007" {
  source = "./k8snode"
  nodename = keys(var.nodes)[5]
  nodeip = values(var.nodes)[5]

  user = var.user
  home = var.home
  sshkeypath = var.sshkeypath

  imageid = var.imageid
  flavorname = var.flavorname
  keypair = "default"
  securitygroups = ["default"]

  clusternodes = var.clusternodes
}

module "node008" {
  source = "./k8snode"
  nodename = keys(var.nodes)[6]
  nodeip = values(var.nodes)[6]

  user = var.user
  home = var.home
  sshkeypath = var.sshkeypath

  imageid = var.imageid
  flavorname = var.flavorname
  keypair = "default"
  securitygroups = ["default"]

  clusternodes = var.clusternodes
}

module "node009" {
  source = "./k8snode"
  nodename = keys(var.nodes)[7]
  nodeip = values(var.nodes)[7]

  user = var.user
  home = var.home
  sshkeypath = var.sshkeypath

  imageid = var.imageid
  flavorname = var.flavorname
  keypair = "default"
  securitygroups = ["default"]

  clusternodes = var.clusternodes
}

module "node010" {
  source = "./k8snode"
  nodename = keys(var.nodes)[8]
  nodeip = values(var.nodes)[8]

  user = var.user
  home = var.home
  sshkeypath = var.sshkeypath

  imageid = var.imageid
  flavorname = var.flavorname
  keypair = "default"
  securitygroups = ["default"]

  clusternodes = var.clusternodes
}

