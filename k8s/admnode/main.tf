# IP Allocation Pools Start 192.168.0.110 - End 192.168.0.200
data "openstack_networking_floatingip_v2" "node001floatingip" {
  address = var.admnodepubip
}

resource "openstack_compute_floatingip_associate_v2" "node001floatingassociation" {
  floating_ip = data.openstack_networking_floatingip_v2.node001floatingip.address
  instance_id = openstack_compute_instance_v2.node001.id
  fixed_ip    = openstack_compute_instance_v2.node001.network.0.fixed_ip_v4
}

resource "openstack_compute_instance_v2" "node001" {
  name            = var.admnode
  image_id        = var.imageid
  flavor_name     = var.flavorname
  key_pair        = var.keypair
  security_groups = var.securitygroups

  network {
    name = "private"
  }
}

resource "null_resource" "provision1" {
  depends_on = [openstack_compute_floatingip_associate_v2.node001floatingassociation]
  connection {
    user = var.user
    private_key = file(var.sshkeypath)
    host = openstack_compute_floatingip_associate_v2.node001floatingassociation.floating_ip
  }

  provisioner "file" {
      source = "scripts/${var.imageid}-networking.sh"
      destination = "${var.home}/${var.user}/${var.imageid}-networking.sh"
  }

  provisioner "file" {
      source = "scripts/helperfunctions.sh"
      destination = "${var.home}/${var.user}/helperfunctions.sh"
  }

  provisioner "file" {
      source = "scripts/${var.imageid}-master.sh"
      destination = "${var.home}/${var.user}/${var.imageid}-master.sh"
  }

  provisioner "file" {
      source = "scripts/joinnodes.sh"
      destination = "${var.home}/${var.user}/joinnodes.sh"
  }

  provisioner "file" {
      source = var.sshkeypath
      destination = "${var.home}/${var.user}/sshkey.pem"
  }

  provisioner "file" {
      source = "k8sconfigs/calico.yaml"
      destination = "${var.home}/${var.user}/calico.yaml"
  }

  provisioner "file" {
      source = "k8sconfigs/rbac-kdd.yaml"
      destination = "${var.home}/${var.user}/rbac-kdd.yaml"
  }

  provisioner "remote-exec" {
      inline = [
        "chmod +x ${var.home}/${var.user}/${var.imageid}-master.sh",
        "${var.home}/${var.user}/${var.imageid}-master.sh ${var.cidr} > ${var.home}/${var.user}/${var.imageid}-master.log 2>&1",
      ]
  }

  # TODO: register the nodes
  provisioner "remote-exec" {
      inline = [
        "chmod +x ${var.home}/${var.user}/joinnodes.sh",
        "chmod 400 *.pem",
        "${var.home}/${var.user}/joinnodes.sh ${join(",", values(var.nodes))} > ${var.home}/${var.user}/joinnodes.log 2>&1",
      ]
  }
}

output "k8sadmin_availability_zone" {
  value = openstack_compute_instance_v2.node001.availability_zone
}

output "k8sadmin_region" {
  value = openstack_compute_instance_v2.node001.region
}

output "k8sadmin_access_ip_v4" {
  value = openstack_compute_instance_v2.node001.access_ip_v4
}

output "k8sadmin_id" {
  value = openstack_compute_instance_v2.node001.id
}

output "k8sadmin_fixed_ip_v4" {
  value = openstack_compute_instance_v2.node001.network[0].fixed_ip_v4
}

output "k8sadmin_floating_ip_v4" {
  value = openstack_compute_instance_v2.node001.network[0].floating_ip
}

output "k8sadmin_mac" {
  value = openstack_compute_instance_v2.node001.network[0].mac
}

output "k8sadmin_port" {
  value = openstack_compute_instance_v2.node001.network[0].port
}

output "k8sadmin_uuid" {
  value = openstack_compute_instance_v2.node001.network[0].uuid
}