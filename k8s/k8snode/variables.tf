variable "nodename" {
  default = {}
}

variable "nodeip" {
  default = {}
}

# Ubuntu 18.04, 2 vCPUs, 3.5GB RAM: 31e62e8f-616f-476b-b39f-01c891691b7d
variable "imageid" {
  default = {}
}

# t2.medium
variable "flavorname" {
  default = {}
}

variable "clusternodes" {
  default = {}
}

# default
variable "keypair" {
  default = {}
}

# ["default"]
variable "securitygroups" {
  default = {}
}

variable "home" {
  default = {}
}

variable "user" {
  default = {}
}

variable "sshkeypath" {
  default = {}
}