data "openstack_networking_floatingip_v2" "floatingip" {
  address = var.nodeip
}

resource "openstack_compute_floatingip_associate_v2" "floatingassociation" {
  floating_ip = data.openstack_networking_floatingip_v2.floatingip.address
  instance_id = openstack_compute_instance_v2.computeinstance.id
  fixed_ip    = openstack_compute_instance_v2.computeinstance.network.0.fixed_ip_v4
}

resource "openstack_compute_instance_v2" "computeinstance" {
  name            = var.nodename
  image_id        = var.imageid
  flavor_name     = var.flavorname
  key_pair        = var.keypair
  security_groups = var.securitygroups

  network {
    name = "private"
  }
}

resource "null_resource" "provision2" {
  depends_on = [openstack_compute_floatingip_associate_v2.floatingassociation]
  connection {
    user = var.user
    private_key = file(var.sshkeypath)
    host = openstack_compute_floatingip_associate_v2.floatingassociation.floating_ip
  }

  provisioner "file" {
      source = "scripts/${var.imageid}-networking.sh"
      destination = "${var.home}/${var.user}/${var.imageid}-networking.sh"
  }

  provisioner "file" {
      source = "scripts/helperfunctions.sh"
      destination = "${var.home}/${var.user}/helperfunctions.sh"
  }

  provisioner "remote-exec" {
      inline = [
        "chmod +x ${var.home}/${var.user}/${var.imageid}-networking.sh",
        "${var.home}/${var.user}/${var.imageid}-networking.sh ${join(",", keys(var.clusternodes))} ${join(",", values(var.clusternodes))} ${openstack_compute_instance_v2.computeinstance.name} > ${var.home}/${var.user}/${var.imageid}-networking.log 2>&1",
      ]
  }
}