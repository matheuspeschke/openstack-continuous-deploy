variable "admnode" {
  default = {}
}

variable "admnodepubip" {
  default = {}
}

variable "imageid" {
  default = {}
}

variable "flavorname" {
  default = {}
}

variable "keypair" {
  default = {}
}

variable "securitygroups" {
  default = {}
}

variable "sshkeypath" {
  default = {}
}

variable "clusternodes" {
  default = {}
}

variable "nodes" {
  default = {}
}

variable "home" {
  default = {}
}

variable "user" {
  default = {}
}

variable "cidr" {
  default = {}
}