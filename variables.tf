variable "user" {
  default = "ubuntu"
}

variable "home" {
  default = "/home"
}

variable "sshkeypath" {
  default = "/home/mpeschke/Documents/Openstack/Multinode POC-Newton/default.pem"
}

variable "admnode" {
  default = "node-001"
}

variable "imageid" {
  default = "4907ba3a-a95b-4bb3-b17e-893dbdd99c14"
}

variable "flavorname" {
  default = "t2.mediumk8s"
}

variable "cidr" {
  default = "192.168.0.0/16"
}

variable "admnodepubip" {
  default = "192.168.0.111"
}

variable "clusternodes" {
  default = {
    "node-001" = "192.168.0.111",
    "node-002" = "192.168.0.112",
    "node-003" = "192.168.0.114",
    "node-004" = "192.168.0.115",
    "node-005" = "192.168.0.116",
    "node-006" = "192.168.0.117",
    "node-007" = "192.168.0.118",
    "node-008" = "192.168.0.119",
    "node-009" = "192.168.0.120",
    "node-010" = "192.168.0.121"
    }
}

variable "nodes" {
  default = {
    "node-002" = "192.168.0.112",
    "node-003" = "192.168.0.114",
    "node-004" = "192.168.0.115",
    "node-005" = "192.168.0.116",
    "node-006" = "192.168.0.117",
    "node-007" = "192.168.0.118",
    "node-008" = "192.168.0.119",
    "node-009" = "192.168.0.120",
    "node-010" = "192.168.0.121"
    }
}