#!/usr/bin/env bash
set -o errexit
set -o nounset

# Install the controller (master)
sudo kubeadm init --pod-network-cidr="${1}"

# Make kubernetes configuration available permanently.
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
export KUBECONFIG=$HOME/.kube/config
echo "export KUBECONFIG=$HOME/.kube/config" >> $HOME/.bashrc

sudo kubectl apply -f rbac-kdd.yaml
sudo kubectl apply -f calico.yaml

# Check status
kubectl get nodes
kubectl get pods --all-namespaces

# Installs the Networking model (in this case, 'Flannel'. Check others at https://kubernetes.io/docs/concepts/cluster-administration/networking/)
# sudo kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml

systemctl status docker.service
systemctl status kubelet.service