#!/usr/bin/env bash
set -o errexit
set -o nounset

source ./helperfunctions.sh

# Error exit codes:
# 1: Adm NodeName, NodeNames or NodeIPs is empty
# 2: NodeNames or NodeIPs is not a comma separated list with at least one item
# 3: NodeNames and NodeIPs don't have the same number of items
# 4: Detected invalid IPv4 in the NodeIPs list
# 5: Detected invalid Hostname in the NodeNames list
# 6: The Adm Node hostname is invalid

K8SNODENAMES="$(trim "${1}")"
K8SNODEIPS="$(trim "${2}")"
K8SADMNODENAME="$(trim "${3}")"

if [ "${K8SNODENAMES}" = "" ]; then
    echo "Expected \${1} as a list of k8s node names, but it's empty."
    exit 1
fi

if [ "${K8SNODEIPS}" = "" ]; then
    echo "Expected \${2} as a list of k8s node IPs, but it's empty."
    exit 1
fi

if [ "${K8SADMNODENAME}" = "" ]; then
    echo "Expected \${3} as the k8s adm node name, but it's empty."
    exit 1
fi

NODENAMESARRAY=($(convertfromnondefaultseparator "${K8SNODENAMES}"))
NODEIPSARRAY=($(convertfromnondefaultseparator "${K8SNODEIPS}"))
NODENAMESARRAYCOUNT=${#NODENAMESARRAY[@]}
NODEIPSARRAYCOUNT=${#NODEIPSARRAY[@]}

if [ "${NODENAMESARRAYCOUNT}" = "0" ]; then
    echo "Expected \${1} as a comma separated list of k8s node names with at least one item."
    exit 2
fi

if [ "${NODEIPSARRAYCOUNT}" = "0" ]; then
    echo "Expected \${2} as a comma separated list of k8s IPs with at least one item."
    exit 2
fi

if [ "${NODEIPSARRAYCOUNT}" != "${NODEIPSARRAYCOUNT}" ]; then
    echo "Expected \${1} and \${2} as lists with the same number of items."
    exit 3
fi

# Add all nodes' entries in the /etc/hosts file.
for index in "${!NODENAMESARRAY[@]}"; do
    VALIDIP="$(validipv4 "${NODEIPSARRAY[${index}]}")"
    if [ "${VALIDIP}" = "" ]; then
        echo "\"${NODEIPSARRAY[${index}]}\" is not a valid IP."
        exit 4
    fi
    VALIDHOSTNAME="$(validhostname "${NODENAMESARRAY[${index}]}")"
    if [ "${VALIDHOSTNAME}" = "" ]; then
        echo "\"${NODENAMESARRAY[${index}]}\" is not a valid hostname."
        exit 5
    fi
    echo "${NODEIPSARRAY[${index}]} ${NODENAMESARRAY[${index}]}" | sudo tee -a /etc/hosts
done

# Add this node's hostname as localhost.
VALIDHOSTNAME="$(validhostname "${K8SADMNODENAME}")"
if [ "${VALIDHOSTNAME}" = "" ]; then
    echo "\"${K8SADMNODENAME}\" is not a valid hostname."
    exit 6
fi
echo "127.0.1.1 ${VALIDHOSTNAME}" | sudo tee -a /etc/hosts