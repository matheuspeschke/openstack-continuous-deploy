#!/usr/bin/env bash
set -o errexit
set -o nounset

# Removes all leading and trailing white spaces.
# ${1}: any string (empty or not)
trim() {
    # remove leading whitespace characters
    local var="${1#"${1%%[![:space:]]*}"}"
    # remove trailing whitespace characters
    echo "${var%"${var##*[![:space:]]}"}"   
}

# Recreates a string with non-default separators (default separator is usually a whitespace)
# into a string with default separators.
# ${1}: string with elements separated by a comma.
convertfromnondefaultseparator() {
    local ARR
    OLDIFS="${IFS}"
    IFS=','
    # Bash:
    read -r -a ARR <<< "${1}"
    # Zsh:
    #read -r -A ARR <<< "${1}"
    IFS="${OLDIFS}"

    echo "${ARR[@]}"
}

# Validates if the string is an IPv4.
# ${1}: any string (empty or not)
validipv4() {
    echo $(echo "${1}" | grep -E -o "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)")
}

# Rules for validating a hostname. See http://tools.ietf.org/html/rfc952 and http://tools.ietf.org/html/rfc1123.
validhostname() {
    # Rule #1: each octet must be up to 63 characters.
    local HOSTN=$(echo "${1}" | grep -E -o "^([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])(\.([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]{0,61}[a-zA-Z0-9]))*$")
    local LENGTH=${#HOSTN}

    # Rule #2: the entire hostname must be up to 255 characters.
    if [ "${LENGTH}" -le "255" ]; then
        echo "${HOSTN}"
    else
        echo ""
    fi
}

# Print the full command for a node to join a cluster.
kubeadmprintjoin() {
    echo "$(kubeadm token create --print-join-command 2>/dev/null)"
}