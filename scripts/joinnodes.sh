#!/usr/bin/env bash
set -o errexit
set -o nounset

source ./helperfunctions.sh

# Example:
# kubeadm join 192.168.43.25:6443 --token 3njb1f.4d4ojqv0gkdpw5g4 --discovery-token-ca-cert-hash sha256:f1763c54d5e85b27c2b0fe8f8ffc93d88bdaff286a4f604a46b67dcb7f7bc4fb
JOIN="$(kubeadmprintjoin)"

echo "Nodes to join K8s: '${1}'"
NODEIPSARRAY=($(convertfromnondefaultseparator ${1}))

# Split the list of ips and ssh to make them join
for index in "${!NODEIPSARRAY[@]}"; do
    VALIDIP="$(validipv4 "${NODEIPSARRAY[${index}]}")"
    if [ "${VALIDIP}" = "" ]; then
        echo "\"${NODEIPSARRAY[${index}]}\" is not a valid IP."
        exit 1
    fi
    echo "Running remote command: ssh -i sshkey.pem ubuntu@${NODEIPSARRAY[${index}]} \"sudo ${JOIN}\""
    echo "*********************************************
JOINING ${VALIDIP}
*********************************************"
    ssh -o StrictHostKeyChecking=no -i sshkey.pem ubuntu@${NODEIPSARRAY[${index}]} "sudo ${JOIN}"
done
