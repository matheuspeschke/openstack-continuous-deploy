terraform {
  required_version = ">= 0.12"
}

# All fields provided by Openstack keystonerc. See README.md.
provider "openstack" {}

module "k8s" {
  source = "./k8s"

  user = var.user
  home = var.home
  sshkeypath = var.sshkeypath

  admnode = var.admnode
  imageid = var.imageid
  flavorname = var.flavorname
  keypair = "default"
  securitygroups = ["default"]

  cidr = var.cidr
  admnodepubip = var.admnodepubip
  clusternodes = var.clusternodes
  nodes = var.nodes
}