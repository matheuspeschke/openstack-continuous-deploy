# Kubernetes using OpenStack

This project deploys a Kubernetes cluster in an OpenStack cloud.

I have an old Newton-version OpenStack, as described here:  
https://www.youtube.com/watch?v=LWrEVZD3nag

In order to use these Terraform scripts with your own OpenStack, you'll need:  

1. The keystone file  
2. The VM's ssh key  
3. An Ubuntu 18 image, with docker, kubeadm and kubelet installed  

## Identify your commits

Example:  
*git config user.name "Matheus Peschke"*  
*git config user.email "software.engineer.22.04@gmail.com"*

## Deployment rationale for this project

### 1. Download and install terraform from https://terraform.io

Version >= 0.12.X

### 2. Import OpenStack credentials

I export the credentials to my OpenStack using the generated keystone file during OpenStack installation:  

*$source $HOME/Documents/Openstack/Multinode\ POC-Newton/keystonerc_admin*

### 3. Initialize the project:

*$terraform init*

### 4. Plan:

*$terraform plan --out current.plan*

(Or you can use a customized set of variables):  

*$terraform plan -var-file="macos.tfvars" --out current.plan*  

### 5. Apply the plan:

*$terraform apply current.plan*

### 6. Recycle when done (Destroy):

*$terraform destroy*  
*$rm \*.plan \*.tfstate \*.tfstate.backup*